package com.example.practicasqlite;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;

public class AgregarJugadoresActivity extends AppCompatActivity {

    EditText txtId, txtNombre;
    Button btnAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_jugadores);
        txtId = (EditText) findViewById(R.id.txtId);
        txtNombre = (EditText) findViewById(R.id.txtNombreJugador);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);


        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });
    }

    private void Agregar() {
        //Establecer conexion con la clase OpenHelper para acceder a la base de datos
        OpenHelper conexion = new OpenHelper(this, "Ballenita", null, 1);
        //Pone a la base de datos en modo de escritura
        SQLiteDatabase db = conexion.getWritableDatabase();

        //Asigna variables de tipo String a los campos de textos
        String id = txtId.getText().toString();
        String nombre = txtNombre.getText().toString();

        //Validacion de campos
        if (!id.isEmpty() && !nombre.isEmpty()){
            //Clase ContentValues para guardar los valores
            ContentValues valores = new ContentValues();
            //Metodo put para agregar al objeto ContentValues
            valores.put("id", id);
            valores.put("nombre", nombre);
            //Inserta a la tabla Jugador los datos que se guardan en el objeto ContentValues
            db.insert("Jugador", null, valores);
            Toast.makeText(this, "Se agregó Jugador", Toast.LENGTH_SHORT).show();
            //Se cierra la base de datos
            db.close();
            txtId.setText("");
            txtNombre.setText("");
        }else if (id.isEmpty()){
            txtId.setError("Campo Obligatorio");
            txtId.requestFocus();
        }else if (nombre.isEmpty()){
            txtNombre.setError("Campo Obligatorio");
            txtNombre.requestFocus();
        }

    }
}
