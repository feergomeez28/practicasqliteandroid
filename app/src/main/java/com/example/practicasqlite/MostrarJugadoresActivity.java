package com.example.practicasqlite;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;
import com.example.practicasqlite.Utilidades.Jugador;

import java.util.ArrayList;

public class MostrarJugadoresActivity extends AppCompatActivity {

    ListView listViewJugadores;
    ArrayList<Jugador> listaJugadores;
    ArrayList<String> listaInformacion;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_jugadores);
        listViewJugadores = (ListView) findViewById(R.id.listViewJugadores);

        consultarJugadores();

        ArrayAdapter adaptador = new ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item, listaInformacion);

        listViewJugadores.setAdapter(adaptador);

        listViewJugadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final CharSequence[] opciones={"Modificar","Eliminar","Cancelar"};
                AlertDialog.Builder alertOpciones = new AlertDialog.Builder(MostrarJugadoresActivity.this);
                alertOpciones.setTitle("Seleccione una opcion");
                alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (opciones[i].equals("Modificar")){
                            Intent intent = new Intent(MostrarJugadoresActivity.this, ModificarActivity.class);
                            intent.putExtra("id", listaJugadores.get(position).getId());
                            intent.putExtra("nombre", listaJugadores.get(position).getNombre());
                            startActivity(intent);
                            finish();
                        }else if (opciones[i].equals("Eliminar")){
                            OpenHelper conexion = new OpenHelper(MostrarJugadoresActivity.this, "Ballenita", null, 1);
                            SQLiteDatabase db = conexion.getWritableDatabase();
                            db.delete("Jugador","id="+ listaJugadores.get(position).getId(),null);
                            db.close();
                            Toast.makeText(MostrarJugadoresActivity.this, "Se eliminó con éxito", Toast.LENGTH_SHORT).show();
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }else {
                            dialog.dismiss();
                        }
                    }
                });alertOpciones.show();
            }
        });

    }

    private void consultarJugadores() {
        OpenHelper conexion = new OpenHelper(this, "Ballenita", null, 1);
        SQLiteDatabase db = conexion.getReadableDatabase();
        listaJugadores = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from Jugador", null);
        while (cursor.moveToNext()){
            Jugador jugador = new Jugador();
            jugador.setId(cursor.getInt(0));
            jugador.setNombre(cursor.getString(1));
            listaJugadores.add(jugador);
        }
        mostrarLista();
    }

    private void mostrarLista() {
        listaInformacion = new ArrayList<>();
        for (int i=0; i<listaJugadores.size(); i++){
            listaInformacion.add(listaJugadores.get(i).getId()
            + " - " + listaJugadores.get(i).getNombre());
        }
    }
}
