package com.example.practicasqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;

public class BuscarJugadorActivity extends AppCompatActivity {

    EditText txtBuscar;
    Button btnBuscar;
    TextView lblId, lblNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_jugador);
        txtBuscar = (EditText) findViewById(R.id.txtBuscar);
        lblId = (TextView) findViewById(R.id.lblId);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        btnBuscar = (Button) findViewById(R.id.btnBusquedaIndividual);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuscarJugador();
            }
        });

    }

    private void BuscarJugador() {
        String id = txtBuscar.getText().toString();
        if (!id.isEmpty()){
            OpenHelper conexion = new OpenHelper(this, "Ballenita", null, 1);
            SQLiteDatabase db = conexion.getReadableDatabase();
            Cursor fila = db.rawQuery("select id, nombre from Jugador where id=" + id,null);
            if (fila.moveToFirst()){
                lblId.setText("ID: "+ fila.getInt(0));
                lblNombre.setText("Nombre: " + fila.getString(1));
                db.close();
            }else {
                Toast.makeText(this, "No existe el jugador", Toast.LENGTH_SHORT).show();
            }
        }else {
            txtBuscar.setError("Tiene que introducir un valor para buscar");
            txtBuscar.requestFocus();
        }
    }
}
