package com.example.practicasqlite;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;

public class ModificarActivity extends AppCompatActivity {

    EditText txtModificarNombre;
    TextView txtModificarId;
    Button btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);
        txtModificarId = (TextView) findViewById(R.id.lblModificarId);
        txtModificarNombre = (EditText) findViewById(R.id.txtModificarNombre);
        btnModificar = (Button) findViewById(R.id.btnModificar);

        final int id = getIntent().getIntExtra("id",0);
        final String nombre = getIntent().getStringExtra("nombre");

        txtModificarId.setText("ID: " + id);
        txtModificarNombre.setText(nombre);

        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModificarJugador(id, nombre);
            }
        });

    }

    private void ModificarJugador(int id, String nombre) {
        OpenHelper conexion = new OpenHelper(this, "Ballenita", null, 1);
        SQLiteDatabase db = conexion.getWritableDatabase();
        nombre = txtModificarNombre.getText().toString();
        if (!nombre.isEmpty()){
            ContentValues modificar = new ContentValues();
            modificar.put("nombre", nombre);
            db.update("Jugador", modificar, "id="+id,null);
            Toast.makeText(this, "Se modificó correctamente el jugador", Toast.LENGTH_SHORT).show();
            db.close();
            startActivity(new Intent(this, MostrarJugadoresActivity.class));
            finish();
        }else if (nombre.isEmpty()){
            txtModificarNombre.setError("Campo Obligatorio");
            txtModificarNombre.requestFocus();
        }
    }

}
